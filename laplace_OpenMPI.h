#ifndef LAPLACE_OPENMPI_INCLUDE
#define LAPLACE_OPENMPI_INCLUDE

    #include <mpi.h>

    #include "laplace.h"


    void solve_plate_OpenMPI_rows(plate *p, double width, double height, unsigned int nx, unsigned int ny, unsigned int it, int ipc, int rank, int size) {

        int displs[size], sendcnts[size];
        divide_work(size, ny, displs, sendcnts);

        double bb = (width*(ny - 1))/(height*(nx - 1));
        bb *= bb;
        double aa = 1/bb;

        MPI_Datatype rowtype, boundary;
        MPI_Type_contiguous(nx, MPI_DOUBLE, &rowtype);
        MPI_Type_commit(&rowtype);
        
        MPI_Datatype inputtype[] = {MPI_DOUBLE, MPI_DOUBLE};
        int blocks[] = {1, 1};
        MPI_Aint lbint, extentint;
        MPI_Type_get_extent(MPI_DOUBLE, &lbint, &extentint);
        MPI_Aint displacement[] = {0, extentint*(nx - 1)};
        MPI_Type_create_struct(2, blocks, displacement, inputtype, &boundary);
        MPI_Type_commit(&boundary);

        MPI_Status status;
        block *b = alloc_block(sendcnts[rank], ipc, nx, rank, size, aa, bb);

        double *a;
        if(rank == 0) {
            a = p->a;
            MPI_Sendrecv(p->a, 1, rowtype, 0, 0, b->a, 1, rowtype, 0, 0, MPI_COMM_WORLD, &status);
            if(size == 1) {
                MPI_Sendrecv(p->a + (ny - 1)*nx, 1, rowtype, 0, 0, b->a + (sendcnts[rank] - 1)*nx, 1, rowtype, 0, 0, MPI_COMM_WORLD, &status);
            } else {
                MPI_Send(p->a + (ny - 1)*nx, 1, rowtype, size - 1, 0, MPI_COMM_WORLD);
            }
        }
        if(size > 1 && rank == size - 1) {
            MPI_Recv(b->a + (sendcnts[rank] - 1)*nx, 1, rowtype, 0, 0, MPI_COMM_WORLD, &status);
        }
        MPI_Scatterv(a, sendcnts, displs, boundary, b->a, sendcnts[rank], boundary, 0, MPI_COMM_WORLD);
        init_block(b);

	//printf("rank %d | Scatterv\n", rank);
        //fflush(stdout);

        //printf("rank %d | %d %d\n", rank, displs[rank], sendcnts[rank]);
        //print_array(b->a2, sendcnts[rank], nx);
        //fflush(stdout);

        int prev = (rank - 1 + size)%size, next = (rank + 1)%size;

        int i = 0;
        while(i < it) {
            MPI_Sendrecv(b->a, ipc, rowtype, prev, rank, b->down, ipc, rowtype, next, next, MPI_COMM_WORLD, &status);
            MPI_Sendrecv(b->a + (sendcnts[rank] - ipc)*nx, ipc, rowtype, next, rank, b->up, ipc, rowtype, prev, prev, MPI_COMM_WORLD, &status);
            update_block(b);
            //printf("rank %d | it %d\n", rank, i);
            //printf("rank: %d\n", rank);
            //print_array(b->up, 2*ipc + sendcnts[rank], nx);
            //fflush(stdout);
            int j = 0;
            while(i < it && j < ipc) {
                solve_block(b, j);
                switch_block(b);
                i++;
                j++;
            }
        }

        MPI_Gatherv(b->a, sendcnts[rank], rowtype, a, sendcnts, displs, rowtype, 0, MPI_COMM_WORLD);

        MPI_Type_free(&rowtype);
        MPI_Type_free(&boundary);
        free_block(b);
        //printf("rank %d | end\n", rank);
        //fflush(stdout);

    }

#endif
