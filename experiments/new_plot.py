import os
import sys
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.pylab as pylab
import matplotlib.colors as mcolors


colors = list(mcolors.TABLEAU_COLORS)

def set_fonts():
    params = {'legend.fontsize': 'x-large',
            'figure.figsize': (10, 6),
            'axes.labelsize': 'xx-large',
            'axes.titlesize':'xx-large',
            'xtick.labelsize':'x-large',
            'ytick.labelsize':'x-large'}
    pylab.rcParams.update(params)


def set_labels(title, xlabel, ylabel, ax):
    leg = ax.legend(loc='best')
    leg_lines = leg.get_lines()
    plt.setp(leg_lines, linewidth=2)
    ax.grid()
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax.set_title(title)


def gen_file_names(folder, tech, ns, vals, vals2):
    names = []
    for n in ns:
        name = '{:s}/res_{:s}_{:d}_{:d}'.format(folder, tech, n, n)
        if vals:
            if vals2:
                for val in vals:
                    for val2 in vals2:
                        names.append(name + '_{}_{}.out'.format(val, val2))
            else:
                for val in vals:
                    names.append(name + '_{}.out'.format(val))
        else:
            names.append(name + '.out')
    return names


def load_OpenMP(file_name):
    mdata = file_name.split('_')
    nx = int(mdata[2])
    if len(mdata) > 5:
        ny, R, S = int(mdata[3]), int(mdata[4]), int(mdata[5].split('.')[0])
    elif len(mdata) > 4:
        ny, R, S = int(mdata[3].split('.')[0]), int(mdata[5].split('.')[0]), None
    else:
        ny, R, S = int(mdata[3].split('.')[0]), None, None
    with open(file_name, 'r') as file:
        i = 0
        data = []
        for line in file:
            line = line.rstrip()
            if line:
                if i == 0:
                    tmp = [int(line.split('=')[1].split(' ')[0])]
                elif i == 5:
                    tmp.append(float(line.split('=')[1][:-3]))
                elif i == 6:
                    tmp.append(float(line.split('=')[1][:-3]))
                elif i == 7:
                    data.append(tmp)
                i = (i + 1) % 8
        return nx, ny, R, S, np.array(data)


def load_MPI(file_name):
    mdata = file_name.split('_')
    nx, ny, ipc = int(mdata[2]),int(mdata[3]), int(mdata[4].split('.')[0])
    with open(file_name, 'r') as file:
        i = 0
        data, tmp = [], []
        for line in file:
            line = line.rstrip()
            if line:
                if i == 0:
                    mdata = line.split(' ')
                    tmp = [int(mdata[0].split('=')[1]), int(mdata[1].split('=')[1])]
                elif i == 5:
                    tmp.append(float(line.split('=')[1][:-3]))
                elif i == 6:
                    tmp.append(float(line.split('=')[1][:-3]))
                elif i == 7:
                    data.append(tmp)
                i = (i + 1) % 8
        return nx, ny, ipc, np.array(data)


def plot_times(names, save_name, linewidth=2):
    set_fonts()
    ax = plt.gca()
    for i, name in enumerate(names):
        mdata = name.split('/')
        color = colors[i]
        if mdata[0] == 'MPI':
            nx, ny, ipc, data = load_MPI(name)
            label = 'nx:{:d} ny:{:d} nodes:1 ipc:{:d}'.format(nx, ny, ipc)
            plt.plot(data[data[:, 0] == 1, 1], data[data[:, 0] == 1, 2], '-o', color=color, label=label, linewidth=linewidth)
            label = 'nx:{:d} ny:{:d} nodes:2 ipc:{:d}'.format(nx, ny, ipc)
            label = ''
            plt.plot(data[data[:, 0] == 2, 1], data[data[:, 0] == 2, 2], 'o', color=color, label=label, linewidth=linewidth)
        else:
            nx, ny, R, S, data = load_OpenMP(name)
            label = 'nx:{:d} ny:{:d} R:{:d} S:{:d}'.format(nx, ny, R, S) if R and S else ('nx:{:d} ny:{:d} R:{:d}'.format(nx, ny, R) if R else 'nx:{:d} ny:{:d}'.format(nx, ny))
            plt.plot(data[:, 0], data[:, 1], '-o', color=color, label=label, linewidth=linewidth)
    ax.set_xticks([1, 2, 4, 8, 16, 32, 64])
    set_labels('čas izvajanja v odvisnosti od števila niti', 'število niti', 'čas [sekunde]', ax)
    #set_labels('čas izvajanja v odvisnosti od števila procesov', 'število procesov', 'čas [sekunde]', ax)
    if save_name:
        plt.savefig(save_name)
    plt.show()


def plot_speedups(names, save_name, linewidth=2):
    set_fonts()
    ax = plt.gca()
    for i, name in enumerate(names):
        mdata = name.split('/')
        color = colors[i]
        if mdata[0] == 'MPI':
            nx, ny, ipc, data = load_MPI(name)
            label = 'nx:{:d} ny:{:d} nodes:1 ipc:{:d}'.format(nx, ny, ipc)
            plt.plot(data[data[:, 0] == 1, 1], data[0, 2]/data[data[:, 0] == 1, 2], '-o', color=color, label=label, linewidth=linewidth)
            label = 'nx:{:d} ny:{:d} nodes:2 ipc:{:d}'.format(nx, ny, ipc)
            label = ''
            plt.plot(data[data[:, 0] == 2, 1], data[0, 2]/data[data[:, 0] == 2, 2], 'o', color=color, label=label, linewidth=linewidth)
        else:
            nx, ny, R, S, data = load_OpenMP(name)
            label = 'nx:{:d} ny:{:d} R:{:d}'.format(nx, ny, R) if R else 'nx:{:d} ny:{:d}'.format(nx, ny)
            plt.plot(data[:, 0], data[0, 1]/data[:, 1], '-o', color=color, label=label, linewidth=linewidth)
    ax.set_xticks([1, 2, 4, 8, 16, 32, 64])
    set_labels('pohitritev v odvisnosti od števila niti', 'število niti', 'pohitritev', ax)
    #set_labels('pohitritev v odvisnosti od števila procesov', 'število procesov', 'pohitritev', ax)
    if save_name:
        plt.savefig(save_name)
    plt.show()


def plot_efficiencies(names, save_name, linewidth=2):
    set_fonts()
    ax = plt.gca()
    for i, name in enumerate(names):
        mdata = name.split('/')
        color = colors[i]
        if mdata[0] == 'MPI':
            nx, ny, ipc, data = load_MPI(name)
            label = 'nx:{:d} ny:{:d} nodes:1 ipc:{:d}'.format(nx, ny, ipc)
            plt.plot(data[data[:, 0] == 1, 1], data[0, 2]/data[data[:, 0] == 1, 2]/data[data[:, 0] == 1, 1], '-o', color=color, label=label, linewidth=linewidth)
            label = 'nx:{:d} ny:{:d} nodes:2 ipc:{:d}'.format(nx, ny, ipc)
            label = ''
            plt.plot(data[data[:, 0] == 2, 1], data[0, 2]/data[data[:, 0] == 2, 2]/data[data[:, 0] == 2, 1], 'o', color=color, label=label, linewidth=linewidth)
        else:
            nx, ny, R, S, data = load_OpenMP(name)
            label = 'nx:{:d} ny:{:d} R:{:d}'.format(nx, ny, R) if R else 'nx:{:d} ny:{:d}'.format(nx, ny)
            plt.plot(data[:, 0], data[0, 1]/data[:, 1]/data[:, 0], '-o', color=color, label=label, linewidth=linewidth)
    ax.set_xticks([1, 2, 4, 8, 16, 32, 64])
    set_labels('učinkovitost v odvisnosti od števila niti', 'število niti', 'učinkovitost', ax)
    #set_labels('učinkovitost v odvisnosti od števila procesov', 'število procesov', 'učinkovitost', ax)
    if save_name:
        plt.savefig(save_name)
    plt.show()


def plot_compare_same(name, names, save_name, linewidth=2):
    set_fonts()
    ax = plt.gca()
    mdata = name.split('/')
    if mdata[0] == 'MPI':
        _, _, _, data_c = load_MPI(name)
    else:
        _, _, _, _, data_c = load_OpenMP(name)
    for i, name in enumerate(names):
        mdata = name.split('/')
        color = colors[i]
        if mdata[0] == 'MPI':
            nx, ny, ipc, data = load_MPI(name)
            if data_c.shape[0] != data.shape[0]:
                data_c = data_c[data_c[:, 1] < 64, :]
            label = 'nx:{:d} ny:{:d} nodes:1 ipc:{:d}'.format(nx, ny, ipc)
            plt.plot(data[data[:, 0] == 1, 1], data_c[data_c[:, 0] == 1, 2]/data[data[:, 0] == 1, 2], '-o', color=color, label=label, linewidth=linewidth)
            label = 'nx:{:d} ny:{:d} nodes:2 ipc:{:d}'.format(nx, ny, ipc)
            label=''
            plt.plot(data[data[:, 0] == 2, 1], data_c[data_c[:, 0] == 2, 2]/data[data[:, 0] == 2, 2], 'o', color=color, label=label, linewidth=linewidth)
        else:
            nx, ny, R, S, data = load_OpenMP(name)
            label = 'nx:{:d} ny:{:d} R:{:d} S:{:d}'.format(nx, ny, R, S) if R and S else ('nx:{:d} ny:{:d} R:{:d}'.format(nx, ny, R) if R else 'nx:{:d} ny:{:d}'.format(nx, ny))
            plt.plot(data[:, 0], data_c[:, 1]/data[:, 1], '-o', color=color, label=label, linewidth=linewidth)
    ax.set_xticks([1, 2, 4, 8, 16, 32, 64])
    set_labels('razmerje v odvisnosti od števila niti', 'število niti', 'čas OpenMP / čas OpenMP RS', ax)
    #set_labels('razmerje v odvisnosti od števila procesov', 'število procesov', 'čas MPI ipc=1 / čas MPI', ax)
    if save_name:
        plt.savefig(save_name)
    plt.show()


def plot_compare_dif(OpenMP_names, MPI_names, save_name, linewidth=2):
    set_fonts()
    ax = plt.gca()
    for i, (OpenMP_name, MPI_name) in enumerate(zip(OpenMP_names, MPI_names)):
        color = colors[i]
        _, _, _, _, data_OpenMP = load_OpenMP(OpenMP_name)
        nx, ny, _, data_MPI = load_MPI(MPI_name)
        label = 'nx:{:d} ny:{:d}'.format(nx, ny)
        print(nx, data_OpenMP.shape, data_MPI.shape)
        plt.plot(data_OpenMP[:, 0], data_OpenMP[:, 1]/data_MPI[data_MPI[:, 0] == 1, 2], '-o', color=color, label=label, linewidth=linewidth)
    ax.set_xticks([1, 2, 4, 8, 16, 32, 64])
    #set_labels('razmerje v odvisnosti od števila niti/procesov', 'število niti/procesov', 'čas OpenMP / čas OpenMP R', ax)
    set_labels('razmerje v odvisnosti od števila niti/procesov', 'število niti/procesov', 'čas OpenMP / čas MPI', ax)
    if save_name:
        plt.savefig(save_name)
    plt.show()


def plot_times2():
    ns = [256, 512, 1024, 2048, 4096, 8192]
    OpenMP_names = gen_file_names('OpenMP', 'OpenMP', ns, [], [])
    MPI_names = gen_file_names('MPI', 'OpenMPI', ns, [1], [])
    set_fonts()
    ax = plt.gca()
    _, _, _, _, OpenMP_256 = load_OpenMP(OpenMP_names[0])
    _, _, _, _, OpenMP_512 = load_OpenMP(OpenMP_names[1])
    _, _, _, _, OpenMP_1024 = load_OpenMP(OpenMP_names[2])
    _, _, _, _, OpenMP_2048 = load_OpenMP(OpenMP_names[3])
    _, _, _, _, OpenMP_4096 = load_OpenMP(OpenMP_names[4])
    _, _, _, _, OpenMP_8192 = load_OpenMP(OpenMP_names[5])
    OpenMP_avg = np.vstack((OpenMP_256[:, 1], OpenMP_512[:, 1], OpenMP_1024[:, 1], OpenMP_2048[:, 1], OpenMP_4096[:, 1], OpenMP_8192[:, 1]))
    #OpenMP_std = np.vstack((OpenMP_256[:, 2], OpenMP_512[:, 2], OpenMP_1024[:, 2], OpenMP_2048[:, 2], OpenMP_4096[:, 2], OpenMP_8192[:, 2]))
    for i, num_threads in enumerate([1, 2, 4, 8, 16, 32, 64]):
        plt.plot(ns, OpenMP_avg[:, i], '-o', color=colors[i], label='št. niti:{:d}'.format(num_threads))
    ax.set_xticks(ns)
    ax.set_xticklabels(ns, rotation='vertical')
    set_labels('čas v odvisnosti od velikosti plošče', 'velikost plošče', 'čas [sekunde]', ax)
    plt.show()

    set_fonts()
    ax = plt.gca()
    _, _, _, MPI_256 = load_MPI(MPI_names[0])
    _, _, _, MPI_512 = load_MPI(MPI_names[1])
    _, _, _, MPI_1024 = load_MPI(MPI_names[2])
    _, _, _, MPI_2048 = load_MPI(MPI_names[3])
    _, _, _, MPI_4096 = load_MPI(MPI_names[4])
    _, _, _, MPI_8192 = load_MPI(MPI_names[5])
    MPI_avg = np.vstack((MPI_256[:, 2], MPI_512[:, 2], MPI_1024[:, 2], MPI_2048[:, 2], MPI_4096[:, 2], MPI_8192[:, 2]))
    #MPI_std = np.vstack((MPI_256[:, 3], MPI_512[:, 3], MPI_1024[:, 3], MPI_2048[:, 3], MPI_4096[:, 3], MPI_8192[:, 3]))
    for i, num_threads in enumerate([1, 2, 4, 8, 16, 32, 64, 32, 64]):
        plt.plot(ns, MPI_avg[:, i], '-o', color=colors[i], label='št. procesov:{:d} št. vozlišč:{:d}'.format(num_threads, 1 if i < 7 else 2))
    
    ax.set_xticks(ns)
    ax.set_xticklabels(ns, rotation='vertical')
    set_labels('čas v odvisnosti od velikosti plošče', 'velikost plošče', 'čas [sekunde]', ax)
    plt.show()


def plot_MPI_nodes_std():
    ns = [256, 512, 1024, 2048, 4096, 8192]

    MPI_names = gen_file_names('MPI', 'OpenMPI', ns, [1], [])
    _, _, _, MPI_256 = load_MPI(MPI_names[0])
    _, _, _, MPI_512 = load_MPI(MPI_names[1])
    _, _, _, MPI_1024 = load_MPI(MPI_names[2])
    _, _, _, MPI_2048 = load_MPI(MPI_names[3])
    _, _, _, MPI_4096 = load_MPI(MPI_names[4])
    _, _, _, MPI_8192 = load_MPI(MPI_names[5])

    nodes_1_256 = MPI_256[np.logical_and(MPI_256[:, 0] == 1, MPI_256[:, 1] >= 32) , :]
    nodes_2_256 = MPI_256[np.logical_and(MPI_256[:, 0] == 2, MPI_256[:, 1] >= 32) , :]

    nodes_1_512 = MPI_512[np.logical_and(MPI_512[:, 0] == 1, MPI_512[:, 1] >= 32) , :]
    nodes_2_512 = MPI_512[np.logical_and(MPI_512[:, 0] == 2, MPI_512[:, 1] >= 32) , :]

    nodes_1_1024 = MPI_1024[np.logical_and(MPI_1024[:, 0] == 1, MPI_1024[:, 1] >= 32) , :]
    nodes_2_1024 = MPI_1024[np.logical_and(MPI_1024[:, 0] == 2, MPI_1024[:, 1] >= 32) , :]

    nodes_1_2048 = MPI_2048[np.logical_and(MPI_2048[:, 0] == 1, MPI_2048[:, 1] >= 32) , :]
    nodes_2_2048 = MPI_2048[np.logical_and(MPI_2048[:, 0] == 2, MPI_2048[:, 1] >= 32) , :]

    nodes_1_4096 = MPI_4096[np.logical_and(MPI_4096[:, 0] == 1, MPI_4096[:, 1] >= 32) , :]
    nodes_2_4096 = MPI_4096[np.logical_and(MPI_4096[:, 0] == 2, MPI_4096[:, 1] >= 32) , :]

    nodes_1_8192 = MPI_8192[np.logical_and(MPI_8192[:, 0] == 1, MPI_8192[:, 1] >= 32) , :]
    nodes_2_8192 = MPI_8192[np.logical_and(MPI_8192[:, 0] == 2, MPI_8192[:, 1] >= 32) , :]

    set_fonts()
    ax = plt.gca()
    plt.plot([0.9, 1.1], [nodes_1_256[0, 2], nodes_2_256[0, 2]], '-', linewidth='1', color=colors[0], label='nx=ny=256')
    plt.errorbar([0.9], nodes_1_256[0, 2], nodes_1_256[0, 3], uplims=True, lolims=True, color=colors[0])
    plt.errorbar([1.1], nodes_2_256[0, 2], nodes_2_256[0, 3], uplims=True, lolims=True, color=colors[0])
    plt.plot([1.9, 2.1], [nodes_1_512[0, 2], nodes_2_512[0, 2]], '-', linewidth='1', color=colors[1], label='nx=ny=512')
    plt.errorbar([1.9], nodes_1_512[0, 2], nodes_1_512[0, 3], uplims=True, lolims=True, color=colors[1])
    plt.errorbar([2.1], nodes_2_512[0, 2], nodes_2_512[0, 3], uplims=True, lolims=True, color=colors[1])
    plt.plot([2.9, 3.1], [nodes_1_1024[0, 2], nodes_2_1024[0, 2]], '-', linewidth='1', color=colors[2], label='nx=ny=1024')
    plt.errorbar([2.9], nodes_1_1024[0, 2], nodes_1_1024[0, 3], uplims=True, lolims=True, color=colors[2])
    plt.errorbar([3.1], nodes_2_1024[0, 2], nodes_2_1024[0, 3], uplims=True, lolims=True, color=colors[2])
    plt.plot([3.9, 4.1], [nodes_1_2048[0, 2], nodes_2_2048[0, 2]], '-', linewidth='1', color=colors[3], label='nx=ny=2048')
    plt.errorbar([3.9], nodes_1_2048[0, 2], nodes_1_2048[0, 3], uplims=True, lolims=True, color=colors[3])
    plt.errorbar([4.1], nodes_2_2048[0, 2], nodes_2_2048[0, 3], uplims=True, lolims=True, color=colors[3])
    plt.plot([4.9, 5.1], [nodes_1_4096[0, 2], nodes_2_4096[0, 2]], '-', linewidth='1', color=colors[4], label='nx=ny=4096')
    plt.errorbar([4.9], nodes_1_4096[0, 2], nodes_1_4096[0, 3], uplims=True, lolims=True, color=colors[4])
    plt.errorbar([5.1], nodes_2_4096[0, 2], nodes_2_4096[0, 3], uplims=True, lolims=True, color=colors[4])
    plt.plot([5.9, 6.1], [nodes_1_8192[0, 2], nodes_2_8192[0, 2]], '-', linewidth='1', color=colors[5], label='nx=ny=8192')
    plt.errorbar([5.9], nodes_1_8192[0, 2], nodes_1_8192[0, 3], uplims=True, lolims=True, color=colors[5])
    plt.errorbar([6.1], nodes_2_8192[0, 2], nodes_2_8192[0, 3], uplims=True, lolims=True, color=colors[5])
    ax.set_xticks([0, 1, 2, 3, 4, 5, 6])
    ax.set_xticklabels(['število vozlišč:', '1     2', '1     2', '1     2', '1     2', '1     2', '1     2'])
    set_labels('čase izvajanj 32 procesov na enem/dveh vozliščih', '', 'čas [sekunde]', ax)
    plt.show()

    set_fonts()
    ax = plt.gca()
    plt.plot([0.9, 1.1], [nodes_1_256[1, 2], nodes_2_256[1, 2]], '-', linewidth='1', color=colors[0], label='nx=ny=256')
    plt.errorbar([0.9], nodes_1_256[1, 2], nodes_1_256[1, 3], uplims=True, lolims=True, color=colors[0])
    plt.errorbar([1.1], nodes_2_256[1, 2], nodes_2_256[1, 3], uplims=True, lolims=True, color=colors[0])
    plt.plot([1.9, 2.1], [nodes_1_512[1, 2], nodes_2_512[1, 2]], '-', linewidth='1', color=colors[1], label='nx=ny=512')
    plt.errorbar([1.9], nodes_1_512[1, 2], nodes_1_512[1, 3], uplims=True, lolims=True, color=colors[1])
    plt.errorbar([2.1], nodes_2_512[1, 2], nodes_2_512[1, 3], uplims=True, lolims=True, color=colors[1])
    plt.plot([2.9, 3.1], [nodes_1_1024[1, 2], nodes_2_1024[1, 2]], '-', linewidth='1', color=colors[2], label='nx=ny=1024')
    plt.errorbar([2.9], nodes_1_1024[1, 2], nodes_1_1024[1, 3], uplims=True, lolims=True, color=colors[2])
    plt.errorbar([3.1], nodes_2_1024[1, 2], nodes_2_1024[1, 3], uplims=True, lolims=True, color=colors[2])
    plt.plot([3.9, 4.1], [nodes_1_2048[1, 2], nodes_2_2048[1, 2]], '-', linewidth='1', color=colors[3], label='nx=ny=2048')
    plt.errorbar([3.9], nodes_1_2048[1, 2], nodes_1_2048[1, 3], uplims=True, lolims=True, color=colors[3])
    plt.errorbar([4.1], nodes_2_2048[1, 2], nodes_2_2048[1, 3], uplims=True, lolims=True, color=colors[3])
    plt.plot([4.9, 5.1], [nodes_1_4096[1, 2], nodes_2_4096[1, 2]], '-', linewidth='1', color=colors[4], label='nx=ny=4096')
    plt.errorbar([4.9], nodes_1_4096[1, 2], nodes_1_4096[1, 3], uplims=True, lolims=True, color=colors[4])
    plt.errorbar([5.1], nodes_2_4096[1, 2], nodes_2_4096[1, 3], uplims=True, lolims=True, color=colors[4])
    plt.plot([5.9, 6.1], [nodes_1_8192[1, 2], nodes_2_8192[1, 2]], '-', linewidth='1', color=colors[5], label='nx=ny=8192')
    plt.errorbar([5.9], nodes_1_8192[1, 2], nodes_1_8192[1, 3], uplims=True, lolims=True, color=colors[5])
    plt.errorbar([6.1], nodes_2_8192[1, 2], nodes_2_8192[1, 3], uplims=True, lolims=True, color=colors[5])
    ax.set_xticks([0, 1, 2, 3, 4, 5, 6])
    ax.set_xticklabels(['število vozlišč:', '1     2', '1     2', '1     2', '1     2', '1     2', '1     2'])
    set_labels('čase izvajanj 64 procesov na enem/dveh vozliščih', '', 'čas [sekunde]', ax)
    plt.show()


#plot_times2() # saved!

ns = [256, 512, 1024, 2048, 4096, 8192]
ipcs = [1, 2, 3, 4, 8]
R = [16, 32, 64, 128, 256, 512]
S = [16, 32, 64, 128, 256, 512]

ns = [1024]
#R = []
S = [512]
#ipcs = [1]

OpenMP_names = gen_file_names('OpenMP', 'OpenMP', ns, [], [])
OpenMP_R_names = gen_file_names('OpenMP-R', 'OpenMP', ns, R, [])
OpenMP_RS_names = gen_file_names('OpenMP-RS', 'OpenMP', ns, R, S)
OpenMP_SR_names = gen_file_names('OpenMP-SR', 'OpenMP', ns, R, S)
MPI_names = gen_file_names('MPI', 'OpenMPI', ns, ipcs, [])

#plot_times(OpenMP_names, None) # saved!
#plot_speedups(OpenMP_names, None) # saved!
#plot_efficiencies(OpenMP_names, None) # saved!

#plot_times(MPI_names, None) # saved!
#plot_speedups(MPI_names, None) # saved!
#plot_efficiencies(MPI_names, None) # saved!

#plot_times(OpenMP_R_names + OpenMP_names, None) # saved!
#plot_times(MPI_names, None) # saved!

#plot_compare_same(OpenMP_names[0], OpenMP_R_names, None) # saved!

#plot_times(OpenMP_SR_names + OpenMP_names, None) # saved!
#plot_compare_same(OpenMP_names[0], OpenMP_RS_names, None)
plot_compare_same(OpenMP_names[0], OpenMP_SR_names, None)

#plot_compare_same(gen_file_names('MPI', 'OpenMPI', ns, [1], [])[0], gen_file_names('MPI', 'OpenMPI', ns, [2, 3, 4], []), None) # saved!

#plot_compare_dif(OpenMP_names, MPI_names, None) # saved!

#plot_MPI_nodes_std()
