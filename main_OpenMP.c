#include <math.h>
#include "laplace_OpenMP.h"


int main(int argc, char *argv[]) {

    if(argc < 9) {
        printf("Missing arguments!\n");
        return 1;
    }
    double width = atof(argv[1]), height = atof(argv[2]);
    unsigned int nx = atoi(argv[3]), ny = atoi(argv[4]);
    unsigned int it = atoi(argv[5]);
    unsigned int repeat = atoi(argv[6]);
    unsigned int R = atoi(argv[7]);
    unsigned int S = atoi(argv[8]);

    double timer[repeat];
    unsigned int i;
    for(i = 0; i < repeat; i++) {
        plate *p = alloc_plate(width, height, nx, ny);
        boundary_north_plate(p, &f_north);
        boundary_south_plate(p, &f_south);
        boundary_east_plate(p, &f_east);
        boundary_west_plate(p, &f_west);

        double start = omp_get_wtime();
        //solve_plate_OpenMP(p, it);
        //solve_plate_OpenMP_RS(p, it, R, S);
	solve_plate_OpenMP_SR(p, it, R, S);
        timer[i] = (omp_get_wtime() - start)/it;

/*
        if(i == repeat - 1) {
            plate *p2 = alloc_plate(width, height, nx, ny);
            boundary_north_plate(p2, &f_north);
            boundary_south_plate(p2, &f_south);
            boundary_east_plate(p2, &f_east);
            boundary_west_plate(p2, &f_west);
            solve_plate(p2, it);
	    //print_plate(p);
	    //print_plate(p2);
            //save_plate(p, "sol_OpenMP.csv");
            printf("Plates are equal: %s\n", (compare_plates(p, p2)) ? "YES" : "NO");
            free_plate(p2);
        }
*/

        free_plate(p);
    }
    double avg_time = 0.0;
    for(i = 0; i < repeat; i++) {
        avg_time += timer[i];
    }
    avg_time /= repeat;
    double std_time = 0.0;
    for(i = 0; i < repeat; i++) {
        double t = avg_time - timer[i];
        std_time += t*t;
    }
    std_time = sqrt(std_time/repeat);

    printf("OpenMP: repeat = %d\n", repeat);
    printf("size = %g x %g\n", width, height);
    printf("points = %u x %u\n", nx, ny);
    printf("it = %u\n", it);
    printf("avg_time_per_iteration = %g sec\n", avg_time);
    printf("std_time_per_iteration = %g sec\n", std_time);
    printf("--------------------------\n");
    
    return 0;
}
