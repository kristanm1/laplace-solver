#include "laplace.h"


plate *alloc_plate(double width, double height, unsigned int nx, unsigned int ny) {
    plate *p = (plate*) malloc(sizeof(plate));
    p->a = (double*) calloc(nx*ny, sizeof(double));
    p->a2 = (double*) calloc(nx*ny, sizeof(double));
    p->width = width;
    p->height = height;
    p->nx = nx;
    p->ny = ny;
    return p;
}

void free_plate(plate *p) {
    free(p->a);
    free(p->a2);
    free(p);
}

void save_plate(plate *p, const char *filename) {
    FILE *f = fopen(filename, "w");
    char line[1 << 16];
    unsigned int i;
    for(i = 0; i < p->ny; i++) {
        unsigned int j, k = i*p->nx;
        unsigned int l = sprintf(line, "%e", p->a[k]);
        for(j = 1; j < p->nx; j++) {
            l += sprintf(&line[l], ":%e", p->a[k + j]);
        }
        fprintf(f, "%s\n", line);
    }
    fclose(f);
}

void print_plate(plate *p) {
    printf("width: %.2g\n", p->width);
    printf("height: %.2g\n", p->height);
    printf("n: [%u x %u]\n", p->nx, p->ny);
    unsigned int i;
    for(i = 0; i < p->ny; i++) {
        unsigned int j;
        for(j = 0; j < p->nx; j++) {
            printf("%9.3g ", p->a[i*p->nx + j]);
        }
        printf("\n");
    }
}

void boundary_north_plate(plate *p, double (*boundary)(double)) {
    double d = p->width/(p->nx - 1);
    double x = 0;
    unsigned int i;
    for(i = 0; i < p->nx; i++) {
        p->a[i] = boundary(x);
        p->a2[i] = p->a[i];
        x += d;
    }
}

void boundary_south_plate(plate *p, double (*boundary)(double)) {
    double d = p->width/(p->nx - 1);
    double x = 0;
    unsigned int i, i0 = (p->ny - 1)*p->nx;
    for(i = 0; i < p->nx; i++) {
        p->a[i0 + i] = boundary(x);
        p->a2[i0 + i] = p->a[i0 + i];
        x += d;
    }
}

void boundary_east_plate(plate *p, double (*boundary)(double)) {
    double d = p->height/(p->ny - 1);
    double y = 0;
    unsigned int i, i0 = p->nx - 1;
    for(i = 0; i < p->ny; i++) {
        p->a[i0] = boundary(y);
        p->a2[i0] = p->a[i0];
        y += d;
        i0 += p->nx;
    }
}

void boundary_west_plate(plate *p, double (*boundary)(double)) {
    double d = p->height/(p->ny - 1);
    double y = 0;
    unsigned int i, i0 = 0;
    for(i = 0; i < p->ny; i++) {
        p->a[i0] = boundary(y);
        p->a2[i0] = p->a[i0];
        y += d;
        i0 += p->nx;
    }
}

void init_boundaries_plate(plate *p) {
    int i;
    for(i = 1; i < p->ny-1; i++) {
        p->a[i*p->nx] = i + 1;
        p->a[(i + 1)*p->nx - 1] = i + 1;
    }
    for(i = 0; i < p->nx; i++) {
        p->a[i] = i + 1;
        p->a[(p->ny - 1)*p->nx + i] = i + 101;
    }
}

void solve_plate(plate *p, unsigned int it) {
    double b = (p->width*(p->ny - 1))/(p->height*(p->nx - 1));
    b *= b;
    double a = 1/b;
    unsigned int i;
    for(i = 0; i < it; i++) {
        unsigned int j;
        for(j = 1; j < p->ny - 1; j++) {
            unsigned int k;
            for(k = 1; k < p->nx - 1; k++) {
                unsigned int l = j*p->nx + k;
                p->a2[l] = ((p->a[l - 1] + p->a[l + 1])/(1 + b) + (p->a[l - p->nx] + p->a[l + p->nx])/(1 + a))/2;
                //p->a2[l] = p->a[l - 1] + p->a[l + 1] + p->a[l - p->nx] + p->a[l + p->nx];
            }
        }
        double *temp = p->a;
        p->a = p->a2;
        p->a2 = temp;
    }
}

int compare_plates(plate *p1, plate *p2) {
    if(p1->width == p2->width && p1->height == p2->height) {
        if(p1->nx == p2->nx && p1->ny == p2->ny) {
            unsigned int i;
            for(i = 0; i < p1->ny; i++) {
                unsigned int j;
                for(j = 0; j < p1->nx; j++) {
                    unsigned int k = i*p1->nx + j;
                    double d = p1->a[k] - p2->a[k];
                    if(d < -EPS || d > EPS) {
                        printf("[%d %d]", i, j);
                        return 0;
                    }
                }
            }
            return 1;
        }
    }
    return 0;
}

double f_north(double x) {
    return x*x;
}

double f_south(double x) {
    return x;
}

double f_east(double x) {
    return cos(2*PI*x);
}

double f_west(double x) {
    return sin(PI*x);
}

// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------

void divide_work(int worker_count, unsigned int array_size, int *displs, int *sendcnts) {
    int i;
    for(i = 0; i < worker_count; i++) {
        displs[i] = i*array_size/worker_count;
        sendcnts[i] = (i + 1)*array_size/worker_count - displs[i];
    }
}

void print_work(int size, int *displs, int *sendcnts) {
    int i;
    for(i = 0; i < size; i++) {
        printf("%2i -> displ: %4d | sencnt: %4d\n", i, displs[i], sendcnts[i]);
    }
}

void print_array(double *array, unsigned int n, unsigned int nx) {
    int i;
    for(i = 0; i < n; i++) {
        int j;
        for(j = 0; j < nx; j++) {
            printf("%9.3g ", array[i*nx + j]);
        }
        printf("\n");
    }
}

block *alloc_block(int sendcnt, int ipc, unsigned int nx, unsigned int rank, unsigned int size, double aa, double bb) {
    block *b = (block*) malloc(sizeof(block));
    b->aa = aa;
    b->bb = bb;
    b->s_row = (rank == 0);
    b->e_row = sendcnt - (rank == size - 1);
    b->sendcnt = sendcnt;
    b->ipc = ipc;
    b->nx = nx;
    b->p = (double*) calloc(2*(sendcnt + 2*ipc)*nx, sizeof(double));
    b->up = b->p;
    b->a = b->up + ipc*nx;
    b->down = b->a + sendcnt*nx;
    b->up2 = b->down + ipc*nx;
    b->a2 = b->up2 + ipc*nx;
    b->down2 = b->a2 + sendcnt*nx;
    return b;
}

void free_block(block *b) {
    free(b->p);
    free(b);
}

void switch_block(block *b) {
    double *temp = b->up;
    b->up = b->up2;
    b->up2 = temp;

    b->a = b->up + b->ipc*b->nx;
    b->down = b->a + b->sendcnt*b->nx;

    b->a2 = b->up2 + b->ipc*b->nx;
    b->down2 = b->a2 + b->sendcnt*b->nx;
}

void init_block(block *b) {
    int i;
    for(i = 1; i < b->sendcnt - 1; i++) {
        int j = i*b->nx;
        b->a2[j] = b->a[j];
        j += b->nx - 1;
        b->a2[j] = b->a[j];
    }
    int j = (b->sendcnt - 1)*b->nx;
    for(i = 0; i < b->nx; i++) {
        b->a2[i] = b->a[i];
        b->a2[j] = b->a[j];
        j++;
    }
}

void update_block(block *b) {
    int i;
    for(i = 0; i < b->ipc*b->nx; i++) {
        b->up2[i] = b->up[i];
        b->down2[i] = b->down[i];
    }
}

void solve_block(block *b, int it) {
    int i;
    
    // solve extra upper part
    for(i = it + 1; i < b->ipc; i++) {
        int j;
        for(j = 1; j < b->nx - 1; j++) {
            int k = i*b->nx + j;
            b->up2[k] = ((b->up[k - 1] + b->up[k + 1])/(1 + b->bb) + (b->up[k - b->nx] + b->up[k + b->nx])/(1 + b->aa))/2;
            //b->up2[k] = b->up[k - 1] + b->up[k + 1] + b->up[k - b->nx] + b->up[k + b->nx];
        }
    }
    
    // solve my part
    for(i = b->s_row; i < b->e_row; i++) {
        int j;
        for(j = 1; j < b->nx - 1; j++) {
            int k = i*b->nx + j;
            b->a2[k] = ((b->a[k - 1] + b->a[k + 1])/(1 + b->bb) + (b->a[k - b->nx] + b->a[k + b->nx])/(1 + b->aa))/2;
            //b->a2[k] = b->a[k - 1] + b->a[k + 1] + b->a[k - b->nx] + b->a[k + b->nx];
        }
    }
    
    // solve extra lower part
    for(i = 0; i < b->ipc - it - 1; i++) {
        int j;
        for(j = 1; j < b->nx - 1; j++) {
            int k = i*b->nx + j;
            b->down2[k] = ((b->down[k - 1] + b->down[k + 1])/(1 + b->bb) + (b->down[k - b->nx] + b->down[k + b->nx])/(1 + b->aa))/2;
            //b->down2[k] = b->down[k - 1] + b->down[k + 1] + b->down[k - b->nx] + b->down[k + b->nx];
        }
    }
    
}

void scatterv(plate *p, int *displs, int *sendcnts, block **procs, int size) {
    int i;
    for(i = 0; i < size; i++) {
        int j;
        for(j = 0; j < sendcnts[i]; j++) {
            int k;
            for(k = 0; k < p->nx; k++) {
                procs[i]->a[j*p->nx + k] = p->a[(displs[i] + j)*p->nx + k];
            }
        }
    }
}

void gatherv(plate *p, int *displs, int *sendcnts, block **procs, int size) {
    int i;
    for(i = 0; i < size; i++) {
        int j;
        for(j = 0; j < sendcnts[i]; j++) {
            int k;
            for(k = 0; k < p->nx; k++) {
                p->a[(displs[i] + j)*p->nx + k] = procs[i]->a[j*p->nx + k];
            }
        }
    }
}

void send(double *r, double *s, int ipc, plate *p) {
    int i;
    for(i = 0; i < ipc*p->nx; i++) {
        r[i] = s[i];
    }
}

void solve_plate_block(plate *p, unsigned int it, int ipc, int size) {

    int displs[size], sendcnts[size];
    divide_work(size, p->ny, displs, sendcnts);

    double bb = (p->width*(p->ny - 1))/(p->height*(p->nx - 1));
    bb *= bb;
    double aa = 1/bb;

    block **procs = (block**) malloc(sizeof(block*) * size);
    int i;
    for(i = 0; i < size; i++) {
        procs[i] = alloc_block(sendcnts[i], ipc, p->nx, i, size, aa, bb);
    }

    scatterv(p, displs, sendcnts, procs, size);

    for(i = 0; i < size; i++) {
        init_block(procs[i]);
    }

    i = 0;
    while(i < it) {

        int rank;
        for(rank = 0; rank < size; rank++) {
            int prev = (rank - 1 + size)%size, next = (rank + 1)%size;
            send(procs[rank]->up, procs[prev]->down - ipc*p->nx , ipc, p);
            send(procs[rank]->down, procs[next]->a, ipc, p);
            update_block(procs[rank]);
        }
        
        for(rank = 0; rank < size; rank++) {

            int j = 0;
            while(i + j < it && j < ipc) {
                solve_block(procs[rank], j);
                switch_block(procs[rank]);

                j++;
            }

        }
        i += ipc;

    }

    gatherv(p, displs, sendcnts, procs, size);

    for(i = 0; i < size; i++) {
        free_block(procs[i]);
    }
    free(procs);

}
