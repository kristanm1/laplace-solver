#!/bin/bash

n_sizes=(256 512 1024 2048 4096)
ipc=(1 2 3 4)
R=(32 64 128 256 512 1024)

ipc=(8)

width=1.0
height=1.0
it=10
repeat=100

for i in ${n_sizes[@]}; do
	#for j in ${R[@]}; do
	#	sh batch_OpenMP.sh $width $height $i $i $it $repeat $j
	#done
	#sh batch_OpenMP.sh $width $height $i $i $it $repeat 0
	#sh batch_OpenMP.sh $width $height $i $i $it $repeat 4096

	for j in ${ipc[@]}; do
		sh batch_OpenMPI.sh $width $height $i $i $it $repeat $j
	done

	echo "done: $i"
done
