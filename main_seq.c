#include "laplace.h"


int main(int argc, char *argv[]) {

    double width = 1.0, height = 1.0;
    unsigned int nx = 100, ny = 100;
    unsigned int it = 1000;

    int ipc = 13, size = 4;
    

    plate *p = alloc_plate(width, height, nx, ny);
    boundary_north_plate(p, &f_north);
    boundary_south_plate(p, &f_south);
    boundary_east_plate(p, &f_east);
    boundary_west_plate(p, &f_west);

    plate *p2 = alloc_plate(width, height, nx, ny);
    boundary_north_plate(p2, &f_north);
    boundary_south_plate(p2, &f_south);
    boundary_east_plate(p2, &f_east);
    boundary_west_plate(p2, &f_west);

    solve_plate(p, it);
    solve_plate_workers(p2, it, ipc, size);

    printf("plates equal: %s\n", compare_plates(p, p2) ? "YES" : "NO");

    //save_plate(p2, "res.csv");

    free_plate(p);
    free_plate(p2);

    return 0;
}
