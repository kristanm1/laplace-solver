#include "laplace_OpenMPI.h"


int main(int argc, char *argv[]) {

    if(argc < 8) {
        printf("Missing arguments[%d < 8]!\n", argc);
        return 1;
    }
   
    int provided;
    MPI_Init_thread(&argc, &argv, MPI_THREAD_SINGLE, &provided);

    int rank, size;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    plate *p;
    unsigned int i, nx, ny, it, repeat, it_per_cycle;
    double width, height, *timer;
    width = atof(argv[1]);
    height = atof(argv[2]);
    nx = atoi(argv[3]);
    ny = atoi(argv[4]);
    it = atoi(argv[5]);
    repeat = atoi(argv[6]);
    it_per_cycle = atoi(argv[7]);

    if(rank == 0) {
        timer = (double*) malloc(repeat*sizeof(double));
    }

    //printf("rank %2d nx %u ny %u it %u repeat %u ipc %u\n", rank, nx, ny, it, repeat, it_per_cycle);
    //fflush(stdout);

    for(i = 0; i < repeat; i++) {
        if(rank == 0) {
            p = alloc_plate(width, height, nx, ny);
            boundary_north_plate(p, &f_north);
            boundary_south_plate(p, &f_south);
            boundary_east_plate(p, &f_east);
            boundary_west_plate(p, &f_west);

	    //init_boundaries_plate(p);

            timer[i] = MPI_Wtime();
        }

        solve_plate_OpenMPI_rows(p, width, height, nx, ny, it, it_per_cycle, rank, size);

        if(rank == 0) {
            timer[i] = (MPI_Wtime() - timer[i])/it;
/*
            if(i == repeat - 1) {
                plate *p2 = alloc_plate(width, height, nx, ny);
                boundary_north_plate(p2, &f_north);
                boundary_south_plate(p2, &f_south);
                boundary_east_plate(p2, &f_east);
                boundary_west_plate(p2, &f_west);
                solve_plate(p2, it);

                printf("equal plates: %s\n", (compare_plates(p, p2)? "YES" : "NO"));
                fflush(stdout);
                free_plate(p2);
            }
*/
            free_plate(p);
        }
    }

    if(rank == 0) {
        double avg_time = 0.0;
        for(i = 0; i < repeat; i++) {
            avg_time += timer[i];
        }
        avg_time /= repeat;
        double std_time = 0.0;
        for(i = 0; i < repeat; i++) {
            double t = avg_time - timer[i];
            std_time += t*t;
        }
        std_time = sqrt(std_time/repeat);
        //free(timer);

        printf("OpenMPI: repeat = %d\n", repeat);
        printf("size = %g x %g\n", width, height);
        printf("points = %u x %u\n", nx, ny);
        printf("it = %u\n", it);
        printf("avg_time_per_iteration = %g sec\n", avg_time);
        printf("std_time_per_iteration = %g sec\n", std_time);
        printf("--------------------------\n");
    }

    MPI_Finalize();

    return 0;
    
}
