#ifndef LAPLACE_INCLUDE
#define LAPLACE_INCLUDE

    #include <stdio.h>
    #include <stdlib.h>
    #include <math.h>


    #define PI 3.14159265359
    #define EPS 1e-10


    typedef struct {
        double *a, *a2, width, height;
        unsigned int nx, ny;
    } plate;

    plate *alloc_plate(double, double, unsigned int, unsigned int);
    void free_plate(plate*);

    void save_plate(plate*, const char*);
    void print_plate(plate*);

    void boundary_north_plate(plate*, double (*)(double));
    void boundary_south_plate(plate*, double (*)(double));
    void boundary_east_plate(plate*, double (*)(double));
    void boundary_west_plate(plate*, double (*)(double));

    void init_boundaries_plate(plate*);

    void solve_plate(plate*, unsigned int);
    int compare_plates(plate*, plate*);

    double f_north(double);
    double f_south(double);
    double f_east(double);
    double f_west(double);

// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------

    void divide_work(int, unsigned int, int*, int*);
    void print_work(int, int*, int*);

    void print_array(double*, unsigned int, unsigned int);

    typedef struct {
        double *p, *a, *a2, aa, bb;
        double *up, *down, *up2, *down2;
        int s_row, e_row, sendcnt, ipc, nx;
    } block;

    block *alloc_block(int, int, unsigned int, unsigned int, unsigned int, double, double);
    void free_block(block*);
    void switch_block(block*);
    void init_block(block*);
    void update_block(block*);
    void solve_block(block*, int);
    void solve_plate_block(plate*, unsigned int, int, int);

#endif
