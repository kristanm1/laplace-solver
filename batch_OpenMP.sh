#!/bin/bash


width=$1
height=$2
nx=$3
ny=$4
it=$5
repeat=$6
R=$7

out=res_OpenMP_"$nx"_"$ny".out
#out=res_OpenMP_"$nx"_"$ny"_"$R".out

./compile_OpenMP.sh

array=(1 2 4 8 16 32 64)
echo '' > "$out"
for i in ${array[@]}; do
    export OMP_PLACES=cores
    export OMP_PROC_BIND=TRUE
    export OMP_NUM_THREADS=$i
    echo "OMP_NUM_THREADS=$OMP_NUM_THREADS R=$R" >> "$out"
    srun --ntasks=1 --cpus-per-task="$i" --reservation=fri --constraint=AMD --job-name=OpenMP main_OpenMP $width $height $nx $ny $it $repeat 0 >> "$out"
    #srun --ntasks=1 --cpus-per-task="$i" --reservation=fri --constraint=AMD --job-name=OpenMP main_OpenMP $width $height $nx $ny $it $repeat $R >> "$out"
    echo "" >> $out
done


