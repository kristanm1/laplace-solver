#!/bin/bash

width=$1
height=$2
nx=$3
ny=$4
it=$5
repeat=$6
it_per_cycle=$7

./compile_OpenMPI.sh

output=res_OpenMPI_"$nx"_"$ny"_"$it_per_cycle".out


num_nodes=1
array=(1 2 4 8 16 32 64)
array=(1 2 4 8 16 32)
echo "" > $output
for num_tasks in ${array[@]}; do
	echo nodes=$num_nodes ntasks=$num_tasks >> $output
	srun --mpi=pmix --nodes=$num_nodes --ntasks=$num_tasks --job-name=OpenMPI --reservation=fri --constraint=AMD \
		--ntasks-per-node=$num_tasks main_OpenMPI $width $height $nx $ny $it $repeat $it_per_cycle >> $output
	echo "" >> $output
done

num_nodes=2
array=(32 64)
array=(32)
for num_tasks in ${array[@]}; do
	ntasks_per_node=$(echo "$num_tasks/$num_nodes" | bc)
	echo nodes=$num_nodes ntasks=$num_tasks >> $output
	#echo "ntasks per node = $ntasks_per_node"
	srun --mpi=pmix --nodes=$num_nodes --ntasks=$num_tasks --job-name=OpenMPI --reservation=fri --constraint=AMD \
		--ntasks-per-node=$ntasks_per_node main_OpenMPI $width $height $nx $ny $it $repeat $it_per_cycle >> $output
	echo "" >> $output
done

