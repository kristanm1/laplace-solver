#ifndef LAPLACE_OPENMP_INCLUDE
#define LAPLACE_OPENMP_INCLUDE

    #include "omp.h"
    #include "laplace.h"


    void solve_plate_OpenMP(plate *p, unsigned int it) {
        double b = (p->width*(p->ny - 1))/(p->height*(p->nx - 1));
        b *= b;
        double a = 1/b;

        #pragma omp parallel shared(p, it, b, a)
        {
        unsigned int size = omp_get_num_threads();
        unsigned int rank = omp_get_thread_num();
        unsigned int start = (rank*p->ny)/size;
        if(rank == 0) {
            start++;
        }
        unsigned int end = ((rank + 1)*p->ny)/size;
        if(rank == size - 1) {
            end--;
        }

        //printf("thread [%d], %d %d\n", rank, start, end);
        
	
        unsigned int i, j, k, l;
        for(i = 0; i < it; i++) {
            //printf("rank: %2u | it: %u [%u, %u]\n", rank, i, start, end);
            for(j = start; j < end; j++) {
                for(k = 1; k < p->nx - 1; k++) {
                    l = j*p->nx + k;
                    p->a2[l] = ((p->a[l - 1] + p->a[l + 1])/(1 + b) + (p->a[l - p->nx] + p->a[l + p->nx])/(1 + a))/2;
                }
            }
            #pragma omp barrier
            #pragma omp single
            {
            double *temp = p->a;
            p->a = p->a2;
            p->a2 = temp;
            //printf("thread [%d] changed array\n", rank);
            }
        }
	
        }
	
    }

    void solve_plate_OpenMP_RS(plate *p, unsigned int it, unsigned int R, unsigned int S) {
        double b = (p->width*(p->ny - 1))/(p->height*(p->nx - 1));
        b *= b;
        double a = 1/b;

        #pragma omp parallel shared(p, it, b, a)
        {
        unsigned int size = omp_get_num_threads();
        unsigned int rank = omp_get_thread_num();
        unsigned int start = (rank*p->ny)/size;
        if(rank == 0) {
            start++;
        }
        unsigned int end = ((rank + 1)*p->ny)/size;
        if(rank == size - 1) {
            end--;
        }
        int i, j, k, l, m, n;
        for(i = 0; i < it; i++) {
            j = start;
            //printf("rank: %d [%d, %d]\n", rank, start, end);
            while(j < end) {
                k = 1;
		while(k < p->nx - 1) {
                    l = 0;
                    
                    while(j+l < end && l < R) {
                        m = 0;
                        while(k+m < p->nx - 1 && m < S) { 
                            //printf("[%2d %2d] ", (j+l), (k+m));
                            n = (j+l)*p->nx + (k+m);
                            p->a2[n] = ((p->a[n - 1] + p->a[n + 1])/(1 + b) + (p->a[n - p->nx] + p->a[n + p->nx])/(1 + a))/2;
                            m++;
			}
                        //printf("\n");
                        l++;
                    }
                    //printf("\n");
                    k += S;
		}
                j += R;
	    }
            #pragma omp barrier
            #pragma omp single
            {
            double *temp = p->a;
            p->a = p->a2;
            p->a2 = temp;
            //printf("thread [%d] changed array\n", rank);
            }
        }
        }

    }

    void solve_plate_OpenMP_SR(plate *p, unsigned int it, unsigned int R, unsigned int S) {
        double b = (p->width*(p->ny - 1))/(p->height*(p->nx - 1));
        b *= b;
        double a = 1/b;

        #pragma omp parallel shared(p, it, b, a)
        {
        unsigned int size = omp_get_num_threads();
        unsigned int rank = omp_get_thread_num();
        unsigned int start = (rank*p->ny)/size;
        if(rank == 0) {
            start++;
        }
        unsigned int end = ((rank + 1)*p->ny)/size;
        if(rank == size - 1) {
            end--;
        }
        int i, j, k, l, m, n;
        for(i = 0; i < it; i++) {
            j = 1;
            //printf("rank: %d [%d, %d]\n", rank, start, end);
            while(j < p->nx - 1) {
                k = start;
		while(k < end) {
                    l = 0;
                    
                    while(k+l < end && l < R) {
                        m = 0;
                        while(j+m < p->nx - 1 && m < S) { 
                            //printf("[%2d %2d] ", (k+l), (j+m));
                            n = (k+l)*p->nx + (j+m);
                            p->a2[n] = ((p->a[n - 1] + p->a[n + 1])/(1 + b) + (p->a[n - p->nx] + p->a[n + p->nx])/(1 + a))/2;
                            m++;
			}
                        //printf("\n");
                        l++;
                    }
                    //printf("\n");
                    k += R;
		}
                j += S;
	    }
/*
            #pragma omp barrier
            #pragma omp single
            {
            double *temp = p->a;
            p->a = p->a2;
            p->a2 = temp;
            //printf("thread [%d] changed array\n", rank);
            }
*/
        }
        }

    }



#endif
